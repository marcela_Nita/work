Accounts Service
=============================

## Business Requirements

Write an endpoint that will allow an existing user to open a savings account. 
The account can only be opened from Monday to Friday within working hours and the user can have only one savings account.
 
The data can be stored however you see fit (e.g. database, in memory, file) and you can use any JVM language and framework.
 
Optional: Create a web form that will provide the data to the backend application.
 
Add a readme file that will explain how to run the application and any other information that you may see relevant.
 
Share the application through github or bitbucket or as zip archive.



## Technical Requirements
* Java 8 or Above


## Code Tour


## Stack
- Spring Boot ( with embedded Tomcat )
- Spring Data JPA (Hibernate)
- Spring Data REST
- PostgreSQL

## Run db dump
Inside the folder sql you will find the banking.sql and user.sql.
Run the sql scripts in order to create manually database 
before to launch the java project from console (terminal)
Set the database credentials in application.yaml file

```
datasource:
    url: jdbc:postgresql://localhost:5432/banking?currentSchema=public
    username: postgres
    password: postgres
    driver-class-name: org.postgresql.Driver
```



## Building with Gradle

Build the application by executing:

	source>$ ./gradlew clean build 
	
Ignore tests 

        source>$ ./gradlew clean build -x test
        
Clean build ignore tests and run embedded tomcat in spring boot

        source>$ ./gradlew clean build -x test bootrun

## Running the Application

	source>$ ./gradlew bootrun

## Available URLs:

	http://localhost:8080/savingAccount
## Test Data
 use a dummy user that is already used in db 
 
 ````
 name : man
 email : man@yahoo.com
 password : 1234$ABcd
````

*Please ignore the naked password in UI and db. The scope here is to show the role of validators
and not the role of security. ( security in a next trial :) )

## CRUD Saving Account :
 Add Saving account
  ```   
   curl -i -X POST -H "Content-Type:application/json" -d '{ "name" : "testName", "email" : "test@yahoo.com" }'	 http://<host>:<port>
 ```

 Main form
 
   ```
    curl http://localhost:8080/savingAccount
```

##Logging
change the path for log files  in application.yaml 

``````
 /Users/marcelanita/Documents/accounts-service/logs/savings-account.log
``````
 
 ## Resources

https://spring.io/guides/gs/handling-form-submission/
https://www.baeldung.com/spring-dynamic-dto-validation
https://spring.io/guides/gs/validating-form-input/
https://stackoverflow.com/questions/33701048/java-check-if-current-date-is-in-range-of-specific-days
https://devwithus.com/exception-handling-for-rest-api-with-spring-boot/
https://www.baeldung.com/exception-handling-for-rest-with-spring
https://www.it-swarm.dev/es/java/como-usar-spring-redirect-si-el-metodo-del-controlador-devuelve-responseentity/1055837055/
https://stackoverflow.com/questions/26805669/how-to-implement-multiple-jsr-303-validation-messages-for-the-same-constraint


## Version Control
Git

https://marcela_Nita@bitbucket.org/marcela_Nita/work.git


