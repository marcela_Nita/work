-- Database: banking

-- DROP DATABASE banking;

CREATE DATABASE banking
  WITH OWNER = postgres
       ENCODING = 'UTF8';
