package ro.banking.accounts.model;

import org.springframework.validation.annotation.Validated;
import ro.banking.accounts.validator.ValidPassword;
import ro.banking.accounts.validator.ValidUserInfo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Validated
@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;



    @Size(min=2, max=30)
    @NotBlank(message = "Name is mandatory")
    private String name;


    @Email
    @NotBlank(message = "Email is mandatory")
    private String email;

    @ValidPassword
//    @Pattern(regexp = AppConstants.PASSWORD_REGEX)
    @NotBlank(message = "Password is mandatory")
    private String password;



    private Integer noOfSavingsAccounts;


    public User(){}


    @ValidUserInfo
    public User(int id, @Size(min = 2, max = 30) @NotBlank(message = "Name is mandatory") String name, @Email @NotBlank(message = "Email is mandatory") String email, @NotBlank(message = "Password is mandatory") String password, Integer noOfSavingsAccounts) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.noOfSavingsAccounts = noOfSavingsAccounts;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public Integer getNoOfSavingsAccounts() {
        return noOfSavingsAccounts;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNoOfSavingsAccounts(Integer noOfSavingsAccounts) {
        this.noOfSavingsAccounts = noOfSavingsAccounts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", noOfSavingsAccounts=").append(noOfSavingsAccounts);
        sb.append('}');
        return sb.toString();
    }
}
