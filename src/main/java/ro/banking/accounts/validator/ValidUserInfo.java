package ro.banking.accounts.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = {UserInfoValidator.class })
@Target({ METHOD,  CONSTRUCTOR, PARAMETER  })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidUserInfo {
    String message() default "The account can only be opened from Monday to Friday within working hours and the user can have only one savings account.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
