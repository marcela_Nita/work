package ro.banking.accounts.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;





public class PasswordValidator implements ConstraintValidator<ValidPassword, String>   {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

        @Override
        public void initialize(ValidPassword constraintAnnotation) {

        }

        @Override
        public boolean isValid(String password, ConstraintValidatorContext context) {
            //check only the length for the simplicity
            return password.length()>3;
        }
    }

