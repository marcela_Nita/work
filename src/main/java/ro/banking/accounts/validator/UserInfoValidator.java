package ro.banking.accounts.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.banking.accounts.model.User;
import ro.banking.accounts.repository.UserInfoRepository;
import ro.banking.accounts.utils.DayOfWeekTimeRange;
import ro.banking.accounts.utils.SavingAccountResponse;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Validator class use to validate some user properties that are not available from
 * request parameters and we need to retrieve them from database. Also validation for the
 * time of opening account can be done here because we cannot link it with a field or method.
 * The error message will be in order of the method calls so if both methods are not validated then
 * the first message will be displayed .
 * We can also send both messages by concatenating them in a single message.
 */
public class UserInfoValidator implements ConstraintValidator<ValidUserInfo, User> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private int noOfSavingsAccounts;

    @Autowired

    private UserInfoRepository userInfoRepository;

    @Override
    public void initialize(final ValidUserInfo userInfo) {
        System.out.println("initialize ......");
    }

    @Override
    public boolean isValid(User user, ConstraintValidatorContext context) {
        logger.debug("validating ......" + user);
        context.disableDefaultConstraintViolation();

        return checkNoOfSavingsAccounts(user, context)
                && checkOpeningAccountTime(user, context);

    }



    // Note: A private method for each constraint decreases the cyclomatic complexity.
    private boolean checkNoOfSavingsAccounts(User user, ConstraintValidatorContext context) {
        // Default validity is true until proven otherwise.
        noOfSavingsAccounts = userInfoRepository.findByNameAndEmailAndPassword(user.getName(),user.getEmail(),user.getPassword()).getNoOfSavingsAccounts();
        logger.debug("no of savings accounts ..." + noOfSavingsAccounts);
        boolean valid = true;

        if (noOfSavingsAccounts>=1) {
            valid = false;
            context.buildConstraintViolationWithTemplate(
                    SavingAccountResponse.NO_OF_ACCOUNTS_EXCEEDED.getDescription()).addConstraintViolation();
        }

        return valid;
    }
    private boolean checkOpeningAccountTime(User user, ConstraintValidatorContext context) {
        // Default validity is true until proven otherwise.
        boolean valid = true;

        if (!isValidTimeForAccount()) {
            valid = false;
            context.buildConstraintViolationWithTemplate(
                    SavingAccountResponse.NOT_IN_WORKING_HOURS.getDescription()).addConstraintViolation();
        }

        return valid;
    }
    private boolean isValidTimeForAccount() {
        DayOfWeekTimeRange range = new DayOfWeekTimeRange(DayOfWeek.MONDAY, LocalTime.of(7, 0), DayOfWeek.FRIDAY, LocalTime.of(17, 0));
        logger.debug("is in range " + range.inRange(LocalDateTime.now() ));
        return range.inRange(LocalDateTime.now());

    }

}
