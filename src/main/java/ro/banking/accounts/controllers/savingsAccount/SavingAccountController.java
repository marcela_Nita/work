package ro.banking.accounts.controllers.savingsAccount;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.banking.accounts.model.User;
import ro.banking.accounts.repository.UserInfoRepository;
import ro.banking.accounts.utils.SavingAccountResponse;
import ro.banking.accounts.utils.Utils;
import ro.banking.accounts.validator.ValidUserInfo;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@Validated
@Controller
public class SavingAccountController implements WebMvcConfigurer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    UserInfoRepository userInfoRepository;


    @Autowired
    SavingAccountController(UserInfoRepository userInfoRepository){
        this.userInfoRepository = userInfoRepository;
    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/savingAccount/results").setViewName("results");
    }

    @GetMapping("/savingAccount")
    public String showForm(Model model, User user, @RequestParam(value = "globalErrorMessage",required = false) String globalErrorMessage,
                           @RequestParam(value = "errorMessage",required = false) @Size(min=2, max=30)String errorMessage) {
       logger.debug("get user  " + user + " errorMessage " + errorMessage + " global error message " + globalErrorMessage);
        if(errorMessage!=null) {
            model.addAttribute("errorMessage", SavingAccountResponse.valueOf(errorMessage).getDescription());
        }
        if(globalErrorMessage!=null) {
            model.addAttribute("globalErrorMessage",globalErrorMessage);// does not make sense to process the error message because this is set in validator
        }
        return "form";
    }


    @PostMapping("/savingAccount")
    String addSavingAccount(@ValidUserInfo @Valid @RequestBody@ModelAttribute final User userfromUI, final BindingResult bindingResult,
                            RedirectAttributes redirectAttrs) {
        logger.debug("post call .....  ");
        User user = userInfoRepository.findByNameAndEmailAndPassword(userfromUI.getName(),userfromUI.getEmail(),userfromUI.getPassword());
        if(user == null){
            redirectAttrs.addAttribute("errorMessage",SavingAccountResponse.USER_NOT_FOUND.name());
            logger.debug("time is ok for creating a saving account ?" + Utils.isValidTimeForCreatingAnAccount());
            return "redirect:/savingAccount";
        }

        //solution done before implement user validator , the error message is sent as request parameter in url path
//        else if( ! Utils.isValidTimeForCreatingAnAccount() ){
//            redirectAttrs.addAttribute("errorMessage",SavingAccountResponse.NOT_IN_WORKING_HOURS.name());
//            System.out.println("time is ok for creating a saving account ?" + Utils.isValidTimeForCreatingAnAccount());
//            return "redirect:/savingAccount";
//        } else if(user.getNoOfSavingsAccounts()!=0 ){
//            redirectAttrs.addAttribute("errorMessage", SavingAccountResponse.NO_OF_ACCOUNTS_EXCEEDED.name());
//            System.out.println("no of savings accounts in db " + user.getNoOfSavingsAccounts());
//            return "redirect:/savingAccount";
//        }

        else if(bindingResult.hasErrors() ){
            logger.debug("other validation errors  ");
           return "redirect:/savingAccount";
        }
        int noOfSavingsAccount = user.getNoOfSavingsAccounts();
        int savingAccountsNow = noOfSavingsAccount + 1 ;
        user.setNoOfSavingsAccounts(savingAccountsNow);
        //updating number of savings account
        userInfoRepository.save(user);
        //if update operation did not work then the error should be processed in a message
        redirectAttrs.addAttribute("infoMessage",SavingAccountResponse.SUCCESS_SAVING_ACCOUNT.name());
        redirectAttrs.addAttribute("userName",user.getName());
        return "redirect:/savingAccount/results";
    }
    @GetMapping("/savingAccount/results")
    public String showResults(Model model,
                              @RequestParam(value = "infoMessage",required = false) String infoMessage,
                              @RequestParam(value = "userName",required = false) String userName) {
        logger.debug("get user name " + userName + " infoMessage " + infoMessage);
        if(infoMessage!=null) {
            model.addAttribute("infoMessage", SavingAccountResponse.valueOf(infoMessage).getDescription());
        }
        model.addAttribute("userName",userName);
        return "results";
    }
}
