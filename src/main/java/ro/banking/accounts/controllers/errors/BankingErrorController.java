package ro.banking.accounts.controllers.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class BankingErrorController implements ErrorController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ERROR_MAPPING = "/error";


    @RequestMapping(value = ERROR_MAPPING)
    public String errorPage( HttpServletRequest req,
                            Model model) {
        Object status = req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        System.out.println("error status " + status);
        if (status != null) {
            logger.debug("error status " + status.toString());
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute("errorMessage", "Page not found");
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                model.addAttribute("errorMessage", "Not allowed");
            }
        }

        return "/error";
    }

    @Override
    public String getErrorPath() {
        return ERROR_MAPPING;
    }
}
