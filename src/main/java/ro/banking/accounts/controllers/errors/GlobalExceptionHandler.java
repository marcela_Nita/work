package ro.banking.accounts.controllers.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;


/**
 * https://devwithus.com/exception-handling-for-rest-api-with-spring-boot/
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConflict(
            RuntimeException ex, WebRequest request) throws URISyntaxException, UnsupportedEncodingException {
        String bodyOfResponse = "This should be application specific";
        HttpHeaders httpHeaders = new HttpHeaders();
        String referer =request.getHeader("referer");
        //exception message should be truncated eg now is addSavingAccount.userfromUI.password: Invalid Password
        //I am still not sure if all the exception messages are the same so I will let it in this form
        // normally it could be created a method to extract the real validation message Invalid Password ( substring maybe )
        String message = ex.getMessage();
        int indexOfEmptySpace = ex.getMessage().indexOf(" ");
       if(indexOfEmptySpace!= -1 && indexOfEmptySpace+1<message.length() ) {
           message = ex.getMessage().substring(indexOfEmptySpace+1);
        }
        URI location = new URI(referer+"?globalErrorMessage="+  URLEncoder.encode(message, "UTF-8"));
        httpHeaders.setLocation(location);//go back to previous page
        //if referer is null then we should redirect to error page
        return handleExceptionInternal(ex, bodyOfResponse,
                httpHeaders, HttpStatus.FOUND, request);
    }



}
