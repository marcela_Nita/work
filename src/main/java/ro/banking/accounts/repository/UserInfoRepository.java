package ro.banking.accounts.repository;

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import ro.banking.accounts.model.User;


public interface UserInfoRepository extends Repository<User, String> {

    User findByNameAndEmailAndPassword(@Param("name") String name, @Param("email") String email,@Param("password") String password);
    void save(User user);
}
