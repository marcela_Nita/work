package ro.banking.accounts.utils;

public enum SavingAccountResponse {
    NOT_IN_WORKING_HOURS("You can create an account only from Monday to Friday between 7 am and 5 pm  !"),
    NO_OF_ACCOUNTS_EXCEEDED("Number of Saving Accounts exceeds! You can have maximum one Saving account !"),
    SUCCESS_SAVING_ACCOUNT("You have created successfully a  saving account!"),
    USER_NOT_FOUND("User not found. Please try again with name , email and password !"),
    INVALID_PASSWORD_LENGTH("Invalid password length");
    private final String description;



    SavingAccountResponse(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

}
