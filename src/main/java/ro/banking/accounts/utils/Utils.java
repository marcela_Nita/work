package ro.banking.accounts.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Utils {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static boolean isValidTimeForCreatingAnAccount() {
        DayOfWeekTimeRange range = new DayOfWeekTimeRange(DayOfWeek.MONDAY, LocalTime.of(7, 0), DayOfWeek.FRIDAY, LocalTime.of(17, 0));
        System.out.println("is in range " + range.inRange(LocalDateTime.now() ));
        return range.inRange(LocalDateTime.now());

    }
}
