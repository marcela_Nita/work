package ro.banking.accounts.utils;

public interface AppConstants {
    public static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,128})";
}
