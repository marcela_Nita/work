-- Database: bankingTest

-- DROP DATABASE bankingTest;

CREATE DATABASE bankingTest
  WITH OWNER = postgres
       ENCODING = 'UTF8';
