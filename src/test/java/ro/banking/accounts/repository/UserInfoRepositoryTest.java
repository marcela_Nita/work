package ro.banking.accounts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ro.banking.accounts.model.User;

import static org.junit.Assert.assertTrue;

@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserInfoRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserInfoRepository userInfoRepository;
    public User givenUser(){
        return new User(2,"man1","man1@yahoo.com","1234$ABcd",1);
    }
    @Test
    public void whenFindByNameThenReturnUser(){
        // given
       User man = givenUser();
       entityManager.merge(man);


        // when
        User found =userInfoRepository.findByNameAndEmailAndPassword(man.getName(),man.getEmail(),man.getPassword());

        // then
        thenReturnUser(found,man);

    }
    public void  thenReturnUser(User found, User man){
      assertTrue(found.getName().equals(man.getName()));
    }
}
