package ro.banking.accounts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author marcelanita
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductApplicationTests {

	@Test
	public void contextLoads() {
	}

}
