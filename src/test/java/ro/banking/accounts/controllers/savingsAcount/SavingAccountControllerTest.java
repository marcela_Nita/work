package ro.banking.accounts.controllers.savingsAcount;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ro.banking.accounts.AbstractTest;
import ro.banking.accounts.model.User;

import static org.junit.Assert.assertEquals;

public class SavingAccountControllerTest  extends AbstractTest {
        @Override
        @Before
        public void setUp() {
            super.setUp();
        }
        @Test
        public void showForm() throws Exception {
            String uri = "/savingAccount";
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

            int status = mvcResult.getResponse().getStatus();
            assertEquals(200, status);

        }
    // not working because the validation should be mocked so that to simulate a dummy object/field that is validated
//        @Test
//        public void updateUser() throws Exception {
//            String uri = "/savingAccount";
//            User user = givenUser();
//            String inputJson = super.mapToJson(user);
//            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
//                    .contentType(MediaType.APPLICATION_JSON_VALUE)
//                    .content(inputJson)).andReturn();
//
//            int status = mvcResult.getResponse().getStatus();
//            assertEquals(200, status);
//            String content = mvcResult.getResponse().getContentAsString();
//            assertEquals(content, "Product is updated successsfully");
//        }


    public User givenUser(){
        return new User(2,"man1","man1@yahoo.com","1234$ABcd",1);
    }
}
